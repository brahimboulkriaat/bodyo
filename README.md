# BodyO

Démonstration de l'écran BodyO

# Testé sur iPhone 12 Pro Max

## Installation

- Clickez sur le bouton 'Télécharger le répertoire' ci-dessus
Ou
- Dans un términal tapez la commande suivante: git clone https://brahimboulkriaat@bitbucket.org/brahimboulkriaat/bodyo.git

- Dans Visual Code : Fichier -> Ouvrir.

- Clickez sur "Run" pour lancer l'application sur un émulateur.

- [Vidéo Demo](https://www.youtube.com/watch?v=dCR_JnZ_I8Y)
