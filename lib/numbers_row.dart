
import 'package:flutter/material.dart';

import 'colors.dart';

class NumbersRow extends StatelessWidget {

  @override
  Widget build(BuildContext context) => Padding(
        padding: EdgeInsets.all(32.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            _buildDataRowItem("assets/icons/heart.png", "85", "likes"),
            _buildDataRowItem("assets/icons/fire.png", "451", "kcal"),
            _buildDataRowItem("assets/icons/chronometer.png", "19", "mins"),
          ],
        ),
      );

      Widget _buildDataRowItem(String asset, String number, String text) => Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Image.asset(
            asset,
            scale: 3,
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Text(
                "$number",
                style: TextStyle(
                  color: Color(COLOR_BLACK),
                  fontWeight: FontWeight.bold,
                  fontSize: 30.0,
                ),
              ),
              SizedBox(width: 8.0),
              Padding(
                padding: EdgeInsets.only(bottom: 4.0),
                child: Text(
                  "$text",
                  style: TextStyle(
                    color: Color(COLOR_BLACK),
                    //fontWeight: FontWeight.bold,
                    //fontSize: 30.0,
                  ),
                ),
              ),
            ],
          ),
        ],
      );
}