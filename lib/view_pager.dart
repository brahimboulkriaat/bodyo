import 'package:flutter/material.dart';
import 'colors.dart';

class ViewPager extends StatelessWidget {
  final PageController pageController;

  ViewPager(this.pageController);

  @override
  Widget build(BuildContext context) => Expanded(
        child: Padding(
          padding: EdgeInsets.only(
            left: 32.0,
            right: 32.0,
          ),
          child: PageView(
            controller: pageController,
            children: [
              _buildPageOne(),
              _buildPageTwo(),
            ],
          ),
        ),
      );

  Widget _buildPageOne() => Wrap(children: [Column(
          children: [
            _buildPageOneRow("1",
                "In a large bowl, sift together the floor baking\npowder salt and sugar. Make a well in the center\nand pour in the milk eggand melted butter, mix\nuntil smooth"),
            SizedBox(height: 32.0),
            _buildPageOneRow("2",
                "Heat a light oiled griddle or frying pan over\nmedium-high eat. Pour or scoop the butter onto\nthe griddle using approximatively 1/4 cup for each\npancake. Brown on both sides and serve hot"),
            SizedBox(height: 32.0),
            _buildPageOneRow("3",
                "Heat a light oiled griddle or frying pan over\nmedium-high eat. Pour or scoop the butter onto\nthe griddle using approximatively 1/4 cup for each\npancake. Brown on both sides and serve hot"),
          
          ],
        )]);

  Widget _buildPageOneRow(String number, String text) => Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            number,
            style: TextStyle(
              color: Color(COLOR_CYAN),
              fontWeight: FontWeight.bold,
              fontSize: 26.0,
            ),
          ),
          SizedBox(
            width: 16.0,
          ),
          Text(
            text,
            style: TextStyle(
              color: Color(COLOR_BLACK),
            ),
          )
        ],
      );

  Widget _buildPageTwo() => Wrap(children: [Column(
        children: [
          _buildPageTwoRow("1 1/2 cups all-purpose flour"),
          _buildPageTwoRow("3 tablespoons butter, melted"),
          _buildPageTwoRow("3 1/2 teaspoons baking powder"),
          _buildPageTwoRow("1 1/4 cups milk"),
          _buildPageTwoRow("1 1/2 cups all-purpose flour"),
          _buildPageTwoRow("3 tablespoons butter, melted"),
        ],
      )]);

  Widget _buildPageTwoRow(String text) => Padding(
        padding: EdgeInsets.only(bottom: 32.0),
        child: Row(
          children: [
            Container(
              width: 10.0,
              height: 10.0,
              decoration: BoxDecoration(
                color: Color(COLOR_CYAN),
                shape: BoxShape.circle,
              ),
            ),
            SizedBox(width: 16.0),
            Text(
              text,
              style: TextStyle(
                color: Color(COLOR_BLACK),
                //fontWeight: FontWeight.bold,
                fontSize: 18.0,
              ),
            ),
          ],
        ),
      );
}
