import 'package:flutter/material.dart';
import 'colors.dart';
import 'text_switch.dart';
import 'numbers_row.dart';
import 'view_pager.dart';

class NavigationBarItem extends StatefulWidget {
  @override
  _NavigationBarItemState createState() => _NavigationBarItemState();
}

class _NavigationBarItemState extends State<NavigationBarItem> {
  bool val = true;

  PageController _pageController = PageController(initialPage: 0);

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) => CustomScrollView(
        slivers: [
          _buildAppBar(),
          _buildContent(),
        ],
      );

  Widget _buildAppBar() => SliverAppBar(
    backgroundColor: Colors.white,
        title: Container(
          width: 40.0,
          height: 40.0,
          child: Icon(
            Icons.chevron_left,
            color: Color(COLOR_BLACK_DARK),
            size: 20.0,
          ),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8.0),
            color: Colors.white,
          ),
        ),
        centerTitle: false,
        actions: [
          PopupMenuItem(
              child: Container(
            width: 40.0,
            height: 40.0,
            child: Icon(
              Icons.format_align_left_outlined,
              color: Color(COLOR_BLACK_DARK),
              size: 20.0,
            ),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8.0),
              color: Colors.white,
            ),
          ))
        ],
        //backgroundColor: Colors.transparent,
        pinned: true,
        expandedHeight: 400.0,
        flexibleSpace: FlexibleSpaceBar(
          title: Text(
            "Colorful pancakes",
            style: TextStyle(
                color: Color(COLOR_BLACK_DARK),
                fontWeight: FontWeight.bold,
                fontSize: 22.0),
          ),
          stretchModes: [
            StretchMode.zoomBackground,
            StretchMode.blurBackground,
            StretchMode.fadeTitle
          ],
          background: Stack(
            fit: StackFit.expand,
            children: [
              Image.asset(
                'assets/pankake.png',
                fit: BoxFit.cover,
              ),
              DecoratedBox(
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    begin: Alignment(0.0, 0.99),
                    end: Alignment(0.0, 0),
                    colors: <Color>[
                      Colors.white,
                      Colors.transparent,
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      );

  Widget _buildContent() => SliverFillRemaining(
        child: Container(
          color: Colors.white,
          child: Column(
            children: [
              NumbersRow(),
              TextSwitch(
                value: false,
                textOn: 'Instructions',
                textOff: 'Ingredients',
                colorOn: Color(COLOR_GREY_LIGHT),
                colorOff: Color(COLOR_GREY_LIGHT),
                iconOn: Icons.done,
                iconOff: Icons.remove_circle_outline,
                textSize: 16.0,
                onChanged: (bool state) {
                  if (_pageController.hasClients) {
                    _pageController.animateToPage(
                      state ? 1 : 0,
                      duration: const Duration(milliseconds: 200),
                      curve: Curves.easeInOut,
                    );
                  }
                },
              ),
              ViewPager(_pageController),
            ],
          ),
        ),
      );  
}
