import 'package:flutter/material.dart';
import 'colors.dart';
import 'navigation_bar_item.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: NavigationBarItem(),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: 3,
        selectedItemColor: Color(COLOR_CYAN),
        unselectedItemColor: Color(COLOR_BLACK),
        showSelectedLabels: true,
        showUnselectedLabels: true,
        backgroundColor: Colors.white,
        type: BottomNavigationBarType.fixed,
        items: <BottomNavigationBarItem>[
          _buildItem("assets/icons/dashboard.png", "Dashboard"),
          _buildItem("assets/icons/kettlebell.png", "Exercices"),
          _buildItem("assets/icons/bubble.png", ""),
          _buildItem("assets/icons/recipe.png", "Recipes"),
          _buildItem("assets/icons/profile.png", "Profile"),
        ],
      ),
    );
  }

  BottomNavigationBarItem _buildItem(String asset, String label) =>
      BottomNavigationBarItem(
        icon: Image.asset(
          asset,
          scale: 3,
        ),
        label: label,
      );
}
