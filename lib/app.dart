import 'package:flutter/material.dart';

import 'home_page.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'BodyO',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        fontFamily: "SoinSansNeue",
      ),
      home: HomePage(),
    );
  }
}