import 'dart:ui';
import 'package:flutter/material.dart';

import 'colors.dart';

class TextSwitch extends StatefulWidget {
  @required
  final bool value;
  @required
  final Function(bool) onChanged;
  final String textOff;
  final String textOn;
  final Color colorOn;
  final Color colorOff;
  final double textSize;
  final Duration animationDuration;
  final IconData iconOn;
  final IconData iconOff;
  final Function onTap;
  final Function onDoubleTap;
  final Function onSwipe;

  TextSwitch(
      {this.value = false,
      this.textOff = "Off",
      this.textOn = "On",
      this.textSize = 14.0,
      this.colorOn = Colors.green,
      this.colorOff = Colors.red,
      this.iconOff = Icons.flag,
      this.iconOn = Icons.check,
      this.animationDuration = const Duration(milliseconds: 100),
      this.onTap,
      this.onDoubleTap,
      this.onSwipe,
      this.onChanged});

  @override
  _TextSwitchState createState() => _TextSwitchState();
}

class _TextSwitchState extends State<TextSwitch>
    with SingleTickerProviderStateMixin {
  AnimationController animationController;
  Animation<double> animation;
  double value = 0.0;

  bool turnState;

  String text = "Instructions";

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    animationController = AnimationController(
        vsync: this,
        lowerBound: 0.0,
        upperBound: 1.0,
        duration: widget.animationDuration);
    animation =
        CurvedAnimation(parent: animationController, curve: Curves.easeInOut);
    animationController.addListener(() {
      setState(() {
        value = animation.value;
      });
    });
    turnState = widget.value;
    _determine();
  }

  @override
  Widget build(BuildContext context) {
    Color transitionColor = Color.lerp(widget.colorOff, widget.colorOn, value);

    return Padding(
      padding: EdgeInsets.only(bottom: 32.0, left: 48.0, right: 48.0),
      child: GestureDetector(
        onDoubleTap: () {
          _action();
          if (widget.onDoubleTap != null) widget.onDoubleTap();
        },
        onTap: () {
          _action();
          if (widget.onTap != null) widget.onTap();
        },
        onPanEnd: (details) {
          _action();
          if (widget.onSwipe != null) widget.onSwipe();
          //widget.onSwipe();
        },
        child: Container(
          padding: EdgeInsets.all(3.0),
          width: 310,
          decoration: BoxDecoration(
              color: transitionColor, borderRadius: BorderRadius.circular(50)),
          child: Stack(
            children: <Widget>[
              Container(
                padding: EdgeInsets.only(right: 32.0),
                alignment: Alignment.centerRight,
                height: 40,
                child: Text(
                  widget.textOff,
                  style: TextStyle(
                      color: Color(COLOR_BLACK),
                      fontWeight: FontWeight.bold,
                      fontSize: widget.textSize),
                ),
              ),
              Container(
                padding: EdgeInsets.only(/*top: 10,*/ left: 32.0),
                alignment: Alignment.centerLeft,
                height: 40,
                child: Text(
                  widget.textOn,
                  style: TextStyle(
                      color: Color(COLOR_BLACK),
                      fontWeight: FontWeight.bold,
                      fontSize: widget.textSize),
                ),
              ),
              Transform.translate(
                offset: Offset(150 * value, 0),
                child: Container(
                  height: 40,
                  width: 155, //TODO: fix
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(50),
                      color: Colors.white),
                  child: Stack(
                    children: <Widget>[
                      /* Center(
                      child: Text(
                        "Ingredients",
                        style: TextStyle(color: transitionColor, fontSize: 16.0,),
                      ),
                    ), */
                      Center(
                        child: Text(
                          text,
                          style: TextStyle(
                            color: Color(COLOR_CYAN),
                            fontSize: 16.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  _action() {
    _determine(changeState: true);
  }

  _determine({bool changeState = false}) {
    setState(() {
      text = turnState ? "Instructions" : "Ingredients";
      if (changeState) turnState = !turnState;
      (turnState)
          ? animationController.forward()
          : animationController.reverse();

      widget.onChanged(turnState);
    });
  }
}
